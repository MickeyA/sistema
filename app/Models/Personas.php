<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Personas extends Model
{
    // use HasFactory;
    protected $fillable = ['nombre', 'tipo_documento', 'num_documento', 'direccion', 'telefono', 'email'];

    public function proveedor(){
        return $this->hasOne(Proveedores::class);
    }

    public function User(){
        return $this->hasOne(User::class);
    }
}
